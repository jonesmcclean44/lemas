from django.urls import path
from . import views

urlpatterns = [
    path('', views.vokabel_list, name='vokabel_list'),
    path('abfrage', views.abfrage, name='abfrage'),
    path('auswertung', views.auswertung, name='auswertung'),

]
