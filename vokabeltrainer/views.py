from django.shortcuts import render
from vokabeltrainer.models import Vokabel
from random import *


# Create your views here.
def vokabel_list(request):
    vokabeln = Vokabel.objects.all()
    return render(request, 'vokabeltrainer/vokabel_list.html', {"vokabeln": vokabeln})


def abfrage(request):
    vokabeln = Vokabel.objects.all()
    zufall_index = randint(0, len(vokabeln) - 1)
    zufall_vokabel = vokabeln[zufall_index]
    zufall_vokabel_deutsch = zufall_vokabel.deutsches_Wort
    return render(request, 'vokabeltrainer/abfrage.html', {"vokabel":zufall_vokabel_deutsch, "index": zufall_index})


def auswertung(request):
    index = request.POST.get("index", -1)
    französisch = request.POST.get("französisch", "")
    vokabeln = Vokabel.objects.all()
    if vokabeln[int(index)].französisches_Wort == französisch:
        antwort = "richtig"
    else:
        antwort = "falsch"
    return render(request, 'vokabeltrainer/auswertung.html', {"antwort": antwort})
