from django.db import models


class Vokabel(models.Model):
    deutsches_Wort = models.CharField(max_length=50)
    französisches_Wort = models.CharField(max_length=50)

    def __str__(self):
        return self.deutsches_Wort
