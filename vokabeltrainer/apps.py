from django.apps import AppConfig


class VokabeltrainerConfig(AppConfig):
    name = 'vokabeltrainer'
